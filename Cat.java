public class Cat{
	private boolean isSleeping;
	private String breed;
	private String name;
	private String currentAction;
	private double speed;
	
	//Constructor
	public Cat(boolean isSleeping, String breed, String name){
		this.isSleeping = isSleeping;
		this.breed = breed;
		this.name = name;
	}
	
	//Setter
	public void setIsSleeping(boolean isSleeping){
		this.isSleeping = isSleeping;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setCurrentAction(String currentAction){
		this.currentAction = currentAction;
	}
	
	public void setSpeed(double speed){
		this.speed = speed;
	}
	
	//getter
	public boolean getIsSleeping(){
		return this.isSleeping;
	}
	
	public String getBreed() {
		return this.breed;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getCurrentAction() {
		return this.currentAction;
	}
	
	public double getSpeed() {
		return this.speed;
	}
	
	//actions
	public void purr(){
		System.out.println(this.name + " - rrrrrr");
	}
	
	public void meow(){
		if(!isSleeping)
			System.out.println(this.name + " - MEOW!!");
	}
	
	public void run(double speed){
		if(isValidSpeed(speed)){
			this.currentAction = "running";
			this.speed = speed;
			System.out.println("cat is running at " + speed);
		}
	}
	
	private boolean isValidSpeed(double speed){
		return (speed > 0);
	}
	
}
